//* Program state processor

use crate::{
    instruction::{ TokenInstruction},
    model::{NftAccount},
};
use solana_program::{
    account_info::{next_account_info, AccountInfo},
    entrypoint::ProgramResult,
    program_error::ProgramError,
    program_pack::{Pack},
    pubkey::Pubkey,
    msg,
};

/// Program state handler.
pub struct Processor {}
impl Processor {
    /// Processes an [Instruction](enum.Instruction.html).
    pub fn process(program_id: &Pubkey, accounts: &[AccountInfo], input: &[u8]) -> ProgramResult {
        let instruction = TokenInstruction::unpack(input)?;

        match instruction {
            TokenInstruction::Mint {
                owner_address,
                meta_data,
            } => {
                msg!("Instruction: InitializeMint");
                Self::process_mint(program_id, accounts, owner_address, meta_data)               
            }
        }
    }
    fn process_mint(program_id: &Pubkey, accounts: &[AccountInfo], owner_address: Pubkey, meta_data: String) -> ProgramResult {
        msg!("At mint processor");
        // Iterating accounts is safer than indexing
        let accounts_iter = &mut accounts.iter();
        let account = next_account_info(accounts_iter)?;
        if account.owner != program_id {
            msg!("Storage allocation not");
            return Err(ProgramError::IncorrectProgramId);
        }
        let nft_account = NftAccount {
            owner_address: owner_address,
            meta_data: meta_data,
            is_initialized: true,
        };
        nft_account.pack_into_slice(&mut account.data.borrow_mut());
        Ok(())
    }
}