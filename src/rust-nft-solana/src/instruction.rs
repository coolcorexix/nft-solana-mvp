use std::str;
use crate::{error::TokenError};
use solana_program::{
    program_error::ProgramError,
    pubkey::Pubkey
};

#[repr(C)]
#[derive(Clone, Debug, PartialEq)]
pub enum TokenInstruction { 
    Mint {        
        owner_address: Pubkey,
        meta_data: String,
    },
}

impl TokenInstruction {
    pub fn unpack(input: &[u8]) -> Result<Self, ProgramError> { 
        use TokenError::InvalidInstruction;
        let (&tag, rest) = input.split_first().ok_or(InvalidInstruction)?;
        Ok(match tag {
            0 => {
                let (owner_address, meta_data_buf) = Self::unpack_pubkey(rest)?;
                let meta_data = match str::from_utf8(meta_data_buf) {
                    Ok(v) => v,
                    Err(_e) => "",
                };
                Self::Mint {
                    owner_address,
                    meta_data: meta_data.to_owned()
                }
                
            }
            _ => return Err(TokenError::InvalidInstruction.into())
        })
    }
    fn unpack_pubkey(input: &[u8]) -> Result<(Pubkey, &[u8]), ProgramError> {
        if input.len() >= 32 {
            let (key, rest) = input.split_at(32);
            let pk = Pubkey::new(key);
            Ok((pk, rest))
        } else {
            Err(TokenError::InvalidInstruction.into())
        }
    }
}