use std::str;
use arrayref::{array_mut_ref, array_ref, array_refs, mut_array_refs};
use solana_program::{
    pubkey::Pubkey,
    program_pack::{IsInitialized, Pack, Sealed},
    program_error::ProgramError,
};


#[derive(Clone, Debug, Default, PartialEq)]
pub struct NftAccount {
    pub owner_address: Pubkey,
    pub meta_data: String,
    /// Is `true` if this structure has been initialized
    pub is_initialized: bool,
}
impl IsInitialized for NftAccount {
    fn is_initialized(&self) -> bool {
        self.is_initialized
    }
}

impl Sealed for NftAccount {}

impl Pack for NftAccount {
    const LEN: usize = 65; // 32 byte of Pubkey + 32 byte of String + 1 byte boolean
    fn unpack_from_slice(src: &[u8]) -> Result<Self, ProgramError> {
        let src = array_ref![src, 0, NftAccount::LEN];
        let (owner_address, meta_data, is_initialized) = array_refs![src, 32, 32, 1];
        let owner_address = Pubkey::new(owner_address);
        let meta_data = match str::from_utf8(meta_data) {
                Ok(v) => v,
                Err(_e) => "",
            };
        let is_initialized = match is_initialized {
            [0] => false,
            [1] => true,
            _ => return Err(ProgramError::InvalidAccountData),
        };
        Ok(NftAccount {
            owner_address,
            meta_data: meta_data.to_owned(),
            is_initialized,
        })
    }
    fn pack_into_slice(&self, dst: &mut [u8]) {
        let dst = array_mut_ref![dst, 0, NftAccount::LEN];
        let (
            owner_address_dst,
            meta_data_dst,
            is_initialized_dst
        ) = mut_array_refs![dst, 32, 32, 1];
        let &NftAccount {
            ref owner_address,
            ref meta_data,
            is_initialized
        } = self;
        owner_address_dst.copy_from_slice(owner_address.as_ref());
        meta_data_dst.copy_from_slice(meta_data.as_ref());
        is_initialized_dst[0] = is_initialized as u8;
    }
}
