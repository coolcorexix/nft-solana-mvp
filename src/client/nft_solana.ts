import {
  PublicKey,
  sendAndConfirmTransaction,
  SystemProgram,
  Transaction,
  TransactionInstruction,
} from '@solana/web3.js';
import fs from 'mz/fs';
import path from 'path';
import {createKeypairFromFile} from './utils';
import * as borsh from 'borsh';
import {connection, payer} from './contexts';

/**
 * Solana NFT's program id
 */
let programId: PublicKey;

/**
 * Path to program files
 */
const PROGRAM_PATH = path.resolve(__dirname, '../../dist/program');

/**
 * Path to program shared object file which should be deployed on chain.
 * This file is created when running either:
 *   - `npm run build:program-c`
 *   - `npm run build:program-rust`
 */
const PROGRAM_SO_PATH = path.join(PROGRAM_PATH, 'solana_nft.so');

/**
 * Path to the keypair of the deployed program.
 * This file is created when running `solana program deploy dist/program/solana_nft.so`
 */
const PROGRAM_KEYPAIR_PATH = path.join(PROGRAM_PATH, 'solana_nft-keypair.json');

class NFT {
  ownerAddress: string;
  hash: string;
  constructor(fields: {ownerAddress: string; hash: string}) {
    this.ownerAddress = fields.ownerAddress;
    this.hash = fields.hash;
  }
}

const NFTSchema = new Map([
  [
    NFT,
    {
      kind: 'struct',
      fields: [
        ['ownerAddress', 'string'],
        ['hash', 'string'],
      ],
    },
  ],
]);

/**
 * Check if the hello world BPF program has been deployed
 */
export async function checkProgram(): Promise<void> {
  // Read program id from keypair file
  try {
    const programKeypair = await createKeypairFromFile(PROGRAM_KEYPAIR_PATH);
    programId = programKeypair.publicKey;
  } catch (err) {
    const errMsg = (err as Error).message;
    throw new Error(
      `Failed to read program keypair at '${PROGRAM_KEYPAIR_PATH}' due to error: ${errMsg}. Program may need to be deployed with \`solana program deploy dist/program/helloworld.so\``,
    );
  }

  // Check if the program has been deployed
  const programInfo = await connection.getAccountInfo(programId);
  if (programInfo === null) {
    if (fs.existsSync(PROGRAM_SO_PATH)) {
      throw new Error(
        'Program needs to be deployed with `solana program deploy dist/program/helloworld.so`',
      );
    } else {
      throw new Error('Program needs to be built and deployed');
    }
  } else if (!programInfo.executable) {
    throw new Error(`Program is not executable`);
  }
  console.log(`Using program ${programId.toBase58()}`);
}

export async function mint(hash: string): Promise<void> {
  const DATA_SLOT_SEED = Date.now().toString();
  const newDataSlotPubKey = await PublicKey.createWithSeed(
    payer.publicKey,
    DATA_SLOT_SEED,
    programId,
  );

  /**
   * The expected size of the NFT.
   */
  const NFT_SIZE = borsh.serialize(
    NFTSchema,
    new NFT({
      ownerAddress: payer.publicKey.toString(),
      hash,
    }),
  ).length;

  const lamports = await connection.getMinimumBalanceForRentExemption(
    NFT_SIZE,
  );
  console.log("🚀 ~ file: nft_solana.ts ~ line 113 ~ mint ~ lamports", lamports)


  const transaction = new Transaction().add(
    SystemProgram.createAccountWithSeed({
      fromPubkey: payer.publicKey,
      basePubkey: payer.publicKey,
      seed: DATA_SLOT_SEED,
      newAccountPubkey: newDataSlotPubKey,
      lamports,
      space: NFT_SIZE,
      programId,
    }),
  );
  await sendAndConfirmTransaction(connection, transaction, [payer]);

  const instruction = new TransactionInstruction({
    keys: [{pubkey: newDataSlotPubKey, isSigner: false, isWritable: true}],
    programId,
    data: Buffer.alloc(0), // All instructions are hellos
  });
  await sendAndConfirmTransaction(
    connection,
    new Transaction().add(instruction),
    [payer],
  );
}


