import { Connection, Keypair, LAMPORTS_PER_SOL } from "@solana/web3.js";
import { getPayer, getRpcUrl } from "./utils";

/**
 * Keypair associated to the fees' payer
 */
export let payer: Keypair;

/**
 * Connection to the network
 */
export let connection: Connection;

/**
 * Establish a connection to the cluster
 */
 export async function establishConnection(): Promise<void> {
    const rpcUrl = await getRpcUrl();
    connection = new Connection(rpcUrl, 'confirmed');
    const version = await connection.getVersion();
    console.log('Connection to cluster established:', rpcUrl, version);
  }
  
  /**
   * Establish an account to pay for everything
   */
  export async function establishPayer(): Promise<void> {
    if (!payer) {
  
      // Calculate the cost to fund the greeter account
  
      payer = await getPayer();
    }
    
    let lamports = await connection.getBalance(payer.publicKey);
    const sig = await connection.requestAirdrop(
      payer.publicKey,
      100000000000000,
    );
    await connection.confirmTransaction(sig);
    lamports = await connection.getBalance(payer.publicKey);
    

    lamports = await connection.getBalance(payer.publicKey);
  
    console.log(
      'Using account',
      payer.publicKey.toBase58(),
      'containing',
      lamports / LAMPORTS_PER_SOL,
      'SOL to pay for fees',
    );
  }
